package com.stevezong.raspberrypi.util;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("wunderlistUtils")
public class WunderlistUtils {
	
	@Value("#{wunderlistApiInfo.xClientID}")	
	private String xClientID;
	
	@Value("#{wunderlistApiInfo.xAccessToken}")	
	private String xAccessToken;
	
	@Value("#{wunderlistApiInfo.task}")
	private String url;
	
	public String getxClientID() {
		return xClientID;
	}

	public void setxClientID(String xClientID) {
		this.xClientID = xClientID;
	}

	public String getxAccessToken() {
		return xAccessToken;
	}

	public void setxAccessToken(String xAccessToken) {
		this.xAccessToken = xAccessToken;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String doGet() throws ClientProtocolException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		String resultJosnStr = null;
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("X-Client-ID", xClientID);
		httpGet.setHeader("X-Access-Token",xAccessToken );
		HttpResponse response = client.execute(httpGet);
		int codeId = response.getStatusLine().getStatusCode();
		if(codeId == 200) {
			HttpEntity resEntity = response.getEntity();
			resultJosnStr = EntityUtils.toString(resEntity);
		}
		client.close();
		return resultJosnStr;
	}
	
}

package com.stevezong.raspberrypi.util;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stevezong.raspberrypi.bean.TaskBean;

public class JsonArrayUtils {
	public static List<TaskBean> getList(String jsonStr){
		Type listType = new TypeToken<LinkedList<TaskBean>>(){}.getType();
		Gson gson = new Gson();
		LinkedList<TaskBean> result = gson.fromJson(jsonStr, listType);
		return result;
	}
	    
}

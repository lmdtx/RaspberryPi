package com.stevezong.raspberrypi.util;

import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.stevezong.raspberrypi.bean.EtouchBean;

public class WeatherUtils {
	public EtouchBean doGet() throws Exception {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet get = new HttpGet("http://wthrcdn.etouch.cn/weather_mini?city=浦东新区");
		CloseableHttpResponse response = httpClient.execute(get);
		HttpEntity entity = response.getEntity();
		String string = EntityUtils.toString(entity, "utf-8");
		Gson gson = new Gson();
		EtouchBean etouch = gson.fromJson(string, EtouchBean.class);
		System.out.println(etouch);
		response.close();
		httpClient.close();
		return etouch;
	}

	public static void main(String[] args) throws Exception {
		// new WeatherUtils().doGet();
		/*
		 * Date date = new Date(); System.out.println(date.getTime());
		 */
	}
}
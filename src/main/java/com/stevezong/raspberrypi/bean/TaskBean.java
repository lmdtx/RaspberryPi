package com.stevezong.raspberrypi.bean;

public class TaskBean{
	String id;
	String assignee_id;
	String created_at;
	String created_by_id;
	String due_date;
	String list_id;
	String revision;
	String starred;
	String title;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAssignee_id() {
		return assignee_id;
	}
	public void setAssignee_id(String assignee_id) {
		this.assignee_id = assignee_id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by_id() {
		return created_by_id;
	}
	public void setCreated_by_id(String created_by_id) {
		this.created_by_id = created_by_id;
	}
	public String getDue_date() {
		return due_date;
	}
	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}
	public String getList_id() {
		return list_id;
	}
	public void setList_id(String list_id) {
		this.list_id = list_id;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getStarred() {
		return starred;
	}
	public void setStarred(String starred) {
		this.starred = starred;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "TaskBean [id=" + id + ", assignee_id=" + assignee_id + ", created_at=" + created_at
				+ ", created_by_id=" + created_by_id + ", due_date=" + due_date + ", list_id=" + list_id
				+ ", revision=" + revision + ", starred=" + starred + ", title=" + title + "]";
	}
}

package com.stevezong.raspberrypi.bean;


import java.io.Serializable;
import java.util.List;

public class EtouchBean implements Serializable {
	private Data data;
	private int status;
	private String desc;
	
	
	
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	
	@Override
	public String toString() {
		return "etouch [data=" + data + ", status=" + status + ", desc=" + desc + "]";
	}



	class Data{
		private Yesterday yesterday;
		private String city;
		private List<Forecast> forecast;
		private String ganmao;
		private String wendu;
		public Yesterday getYesterday() {
			return yesterday;
		}
		public void setYesterday(Yesterday yesterday) {
			this.yesterday = yesterday;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public List<Forecast> getForecast() {
			return forecast;
		}
		public void setForecast(List<Forecast> forecast) {
			this.forecast = forecast;
		}
		public String getGanmao() {
			return ganmao;
		}
		public void setGanmao(String ganmao) {
			this.ganmao = ganmao;
		}
		public String getWendu() {
			return wendu;
		}
		public void setWendu(String wendu) {
			this.wendu = wendu;
		}
		@Override
		public String toString() {
			return "Data [yesterday=" + yesterday + ", city=" + city + ", forecast=" + forecast + ", ganmao=" + ganmao
					+ ", wendu=" + wendu + "]";
		}
		
		
	}
	
	class Yesterday{
		private String date;
		private String high;
		private String fx;
		private String low;
		private String fl;
		private String type;
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getHigh() {
			return high;
		}
		public void setHigh(String high) {
			this.high = high;
		}
		public String getFx() {
			return fx;
		}
		public void setFx(String fx) {
			this.fx = fx;
		}
		public String getLow() {
			return low;
		}
		public void setLow(String low) {
			this.low = low;
		}
		public String getFl() {
			return fl;
		}
		public void setFl(String fl) {
			this.fl = fl;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		@Override
		public String toString() {
			return "Yesterday [date=" + date + ", high=" + high + ", fx=" + fx + ", low=" + low + ", fl=" + fl
					+ ", type=" + type + "]";
		}
		
		
	}
	class Forecast{
		private String date;
		private String high;
		private String fengli;
		private String low;
		private String fengxiang;
		private String type;
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getHigh() {
			return high;
		}
		public void setHigh(String high) {
			this.high = high;
		}
		public String getFengli() {
			return fengli;
		}
		public void setFengli(String fengli) {
			this.fengli = fengli;
		}
		public String getLow() {
			return low;
		}
		public void setLow(String low) {
			this.low = low;
		}
		public String getFengxiang() {
			return fengxiang;
		}
		public void setFengxiang(String fengxiang) {
			this.fengxiang = fengxiang;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		@Override
		public String toString() {
			return "Forecast [date=" + date + ", high=" + high + ", fengli=" + fengli + ", low=" + low + ", fengxiang="
					+ fengxiang + ", type=" + type + "]";
		}
		
		
	}
}

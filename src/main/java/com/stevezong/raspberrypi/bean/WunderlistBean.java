package com.stevezong.raspberrypi.bean;

public class WunderlistBean {
	private String id;
	private String created_at;
	private String title;
	private String list_type;
	private String type;
	private String revision;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getList_type() {
		return list_type;
	}
	public void setList_type(String list_type) {
		this.list_type = list_type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	@Override
	public String toString() {
		return "WunderlistBean [id=" + id + ", created_at=" + created_at + ", title=" + title + ", list_type="
				+ list_type + ", type=" + type + ", revision=" + revision + "]";
	}
	
	
}

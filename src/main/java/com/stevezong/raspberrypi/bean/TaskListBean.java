package com.stevezong.raspberrypi.bean;

import java.util.List;

public class TaskListBean {
	
	private List<TaskBean> list;
	
	
	
	
	public List<TaskBean> getList() {
		return list;
	}




	public void setList(List<TaskBean> list) {
		this.list = list;
	}




	@Override
	public String toString() {
		return "TaskListBean [list=" + list + "]";
	}


	
}

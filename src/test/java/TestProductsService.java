

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.LinkedList;

import javax.annotation.Resource;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stevezong.raspberrypi.bean.TaskBean;
import com.stevezong.raspberrypi.util.JsonArrayUtils;
import com.stevezong.raspberrypi.util.WunderlistUtils;


public class TestProductsService {

	private ApplicationContext ac;
	
	@Resource(name="wunderlistUtils")
	WunderlistUtils wunderlistUtils;

	@Before
	public void init() {
		String[] conf = {"conf/spring-mvc.xml","conf/spring-aop.xml","conf/spring-utilInfo.xml"};
		ac = new ClassPathXmlApplicationContext(conf);
		wunderlistUtils = ac.getBean("wunderlistUtils",WunderlistUtils.class);
	}

	
	@Test	//用列 2：查询
	public void t2() {
		System.out.println(wunderlistUtils.getxClientID()+":"+wunderlistUtils.getxAccessToken());
	}
	
	@Test	//用列 2：查询
	public void t3() {
		try {
			
			String result = wunderlistUtils.doGet();
			System.out.println(JsonArrayUtils.getList(result));
			
			
			
			
		/*	Gson gson = new Gson();
			Type listType = new TypeToken<LinkedList<TaskBean>>(){}.getType();
			LinkedList<TaskBean> resultList = gson.fromJson(result, listType);
			System.out.println(resultList);*/
			//System.out.println(wunderlistUtils.doGet("http://a.wunderlist.com/api/v1/tasks?list_id=354770392"));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test	//用列 2：查询
	public void t4() throws ClientProtocolException, IOException {
	//	System.out.println(wunderlistUtils.getxClientID()+":"+wunderlistUtils.getxAccessToken());
		String url = "http://a.wunderlist.com/api/v1/notes:";
		//System.out.println(wunderlistUtils.doGet(url));
		String resultJsonStr  = wunderlistUtils.doGet(url);
		System.out.println(resultJsonStr);

	}
	

	
}
